<?php
  $title = "envs.net | privacy-policy";
  $desc = "envs.net | privacy-policy";

include 'header.php';
?>

  <body id="body" class="dark-mode">
    <div>

      <div class="button_back">
        <pre class="clean"><strong><a href="/">&lt; back</a></strong></pre>
      </div>

      <div id="main">
<div class="block">
<h1><em>privacy policy</em></h1>
<p></p>
</div>

<pre>
envs.net takes privacy seriously and, as such, remains committed
to being in compliance with the GDPR, which took effect may 25, 2018.
</pre>
<br />

<h2>&#35; what data do we collect from you?</h2>
<pre>
envs.net collects your email address during signup and any IP addresses
you sign in to the system from in accordance with technical requirements.

we also store whatever data you create or maintain in your home directory
from your envs.net account.
</pre>
<br />

<h2>&#35; how long do we retain data?</h2>
<pre>
your email address is retained only long enough to process your account creation
and notify you of that completion. IPs are retained for a maximum of 90 days.
</pre>
<br />

<h2>&#35; how can i request removal of my information?</h2>
<pre>
your user data is protected by the unix permissions model. by default,
in the sharing nature of a public access unix system, your files are
visible to other users. you may, however, set the access however you like.

contact <a href="mailto:sudoers@envs.net">sudoers&#64;envs.net</a> for any further requests.
</pre>
      </div>

<?php include 'footer.php'; ?>
